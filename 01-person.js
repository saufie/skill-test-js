var person = {
    name : "Muhammad Saufi",
    address : "Kotabaru, Kalimantan Selatan",
    hobby : [
        "Game Online",
        "Travelling",
        "Bulutangkis"
    ],
    isMarried : false,
    school : {
        highSchool : "SMKN 1 Kotabaru",
        university : "Universitas Amikom Yogyakarta",
    },
    skill : {
        webProgramming : [
            "html",
            "javascript",
            "php",
            "ruby on rails",
            "codeigniter"
        ],
        other : [
            "photoshop",
            "corel draw",
            "after effect",
            "premiere pro"
        ]
    }
};
console.log(person);