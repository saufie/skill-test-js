var request = require("request");

var options = { method: 'POST',
  url: 'https://gis.locatorlogic.com/arcgis/tokens/',
  headers:
  { 'content-type': 'application/x-www-form-urlencoded' },
  form:
  { client_id: 'CLIENT_ID_FROM_YOUR_APPLICATION',
    client_secret: 'CLIENT_SECRET_FROM_YOUR_APPLICATION',
    grant_type: 'client_credentials' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});